package com.devcamp.pizza365.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.pizza365.entity.Customer;

public class ExcelExporter {
    private Workbook workbook;
    private Sheet activeSheet;
    private List<Customer> customers;

    public ExcelExporter(List<Customer> customers) {
        this.workbook = new XSSFWorkbook();
        this.activeSheet = this.workbook.createSheet("customers");

        this.customers = customers;
    }

    public void writeCell(Row row, int indexColumn, Object value, CellStyle style) {
        this.activeSheet.autoSizeColumn(indexColumn);
        Cell cell = row.createCell(indexColumn);
        cell.setCellStyle(style);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
        
    }

    public void writeHeaders() {
        // Create first row for header
        Row row = this.activeSheet.createRow(0);
        // Add Style
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = (XSSFFont) this.workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.BLUE.getIndex());
        style.setFont(font);

        // Write headers Cell
        this.writeCell(row, 0, "User Id",style);
        this.writeCell(row, 1, "First Name",style);
        this.writeCell(row, 2, "Last Name",style);
        this.writeCell(row, 3, "Phone Number",style);
        this.writeCell(row, 4, "Address",style);
        this.writeCell(row, 5, "City",style);
        this.writeCell(row, 6, "Country",style);
        this.writeCell(row, 7, "Post Code",style);

    }

    public void writeDataLines(){
        int rowCount = 1;

        for(Customer customer: this.customers) {
              // Create first row for header
            Row row = this.activeSheet.createRow(rowCount);
            CellStyle style = this.workbook.createCellStyle();

            // Write data Cell
            this.writeCell(row, 0, customer.getId(),style);
            this.writeCell(row, 1, customer.getFirstName(),style);
            this.writeCell(row, 2, customer.getLastName(),style);
            this.writeCell(row, 3, customer.getPhoneNumber(),style);
            this.writeCell(row, 4, customer.getAddress(),style);
            this.writeCell(row, 5, customer.getCity(),style);
            this.writeCell(row, 6, customer.getCountry(),style);
            this.writeCell(row, 7, customer.getPostalCode(),style);

            rowCount++;
        }
    }

    public void export(OutputStream stream) throws IOException {    
        // Write headers
        this.writeHeaders();
        // Write data lines
        this.writeDataLines(); 
        
        this.workbook.write(stream);
        stream.close();
    }
}
